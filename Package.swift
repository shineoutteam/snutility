// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SNUtility",
    platforms: [
        .iOS(.v10),
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "SNUtility",
            targets: ["SNUtility"]),
        .library(
        name: "SNUtilityView",
        targets: ["SNUtilityView"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "git@bitbucket.org:shineoutteam/snlanguage.git", Package.Dependency.Requirement.branchItem("master")),
        .package(url: "git@bitbucket.org:shineoutteam/snextension.git", Package.Dependency.Requirement.branchItem("master")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "SNUtility",
            dependencies: ["SNLanguage", "SNExtension"]),
        .target(
        name: "SNUtilityView",
        dependencies: ["SNLanguage"]),
    ]
)
