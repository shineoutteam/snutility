//
//  File.swift
//  
//
//  Created by Saran Nonkamjan on 23/9/2562 BE.
//  Copyright © 2561 Saran Nonkamjan. All rights reserved.
//

import Foundation

/**
 ภายใน block นี้จะทำงานใน main queue

 - Parameters:
    - closure: เขียนการทำงานที่จะทำงานใน main queue
 */
@available(iOS 10.0, *)
public func mainQueue(closure:@escaping ()->()) {
    delay(sec: 0, closure: closure)
}

/**
 ภายใน block นี้จะทำงานใน main queue โดยทำงานหลังจากผ่านเวลาไปตามที่กำหนด

 - Parameters:
 - delay: หน่วงเวลา DispatchTime
 - closure: เขียนการทำงานที่จะทำงานใน main queue
 */
@available(iOS 10.0, *)
public func delay(_ delay: DispatchTime, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: delay) {
        closure()
    }
}

/**
 ภายใน block นี้จะทำงานใน main queue โดยทำงานหลังจากผ่านเวลาไปตามที่กำหนด

 - Parameters:
    - sec: หน่วงเวลาเป็นหน่วยวินาที
    - closure: เขียนการทำงานที่จะทำงานใน main queue
 */
@available(iOS 10.0, *)
public func delay(sec: Int, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(sec)) {
        closure()
    }
}

/**
 ภายใน block นี้จะทำงานใน main queue โดยทำงานหลังจากผ่านเวลาไปตามที่กำหนด

 - Parameters:
 - millisec: หน่วงเวลา millisec
 - closure: เขียนการทำงานที่จะทำงานใน main queue
 */
@available(iOS 10.0, *)
public func delay(millisec: Int, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(millisec)) {
        closure()
    }
}
