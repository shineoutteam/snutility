//
//  Utility.swift
//
//  Created by Saran Nonkamjan on 3/3/2560 BE.
//  Copyright © 2560 ShineOut. All rights reserved.
//

#if canImport(UIKit)

import Foundation

public struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

// ถ้าเป็น True คือ Store ใหม่กว่า ในเครื่อง
public func checkVersion(current: String, store: String) -> Bool {
    let versionStore = store.split(separator: ".")
    let projectVersion = current.split(separator: ".")

    print$("VersionStore: \(versionStore)\nProjectVersion: \(projectVersion)")

    for index in 0...2 {
        if "\(versionStore[index])".CGFloatValue > "\(projectVersion[index])".CGFloatValue {
            // Store มากกว่า Bundle
            return true
        } else if "\(versionStore[index])".CGFloatValue < "\(projectVersion[index])".CGFloatValue {
            // Bundle มากกว่า Store
            return false
        }
    }

    // Equal
    return false
}

public func checkThaiCardID(cardNumber: NSInteger) -> Bool {
    let cardString = "\(cardNumber)"
    if cardString.count != 13 {
        // ไม่ใช่ 13 หลัก
        return false
    }

    var sumValue: NSInteger = 0
    var bitCheck: NSInteger = 0
    // ขั้นตอนที่ 1 - เอาเลข 12 หลักมา เขียนแยกหลักกันก่อน (หลักที่ 13 ไม่ต้องเอามานะคร้าบ)
    cardString.enumerated().forEach { (value) in

        let (offset, element) = value

        if offset == 12 {
            bitCheck = "\(element)".integerValue
        } else {
            // ขั้นตอนที่ 2 - เอาเลข 12 หลักนั้นมา คูณเข้ากับเลขประจำหลักของมัน
            // ตัวคูณ 13 12 11 10 9 8 7 6 5 4 3 2
            let index = 13 - offset
            // ขั้นตอนที่ 3 - เอาผลคูณทั้ง 12 ตัวมา บวกกันทั้งหมด จะได้ 13+24+0+10+45+32+7+24+30+8+6+6=205
            sumValue += index * "\(element)".integerValue
        }
    }

    // ขั้นตอนที่ 4 - เอาเลขที่ได้จากขั้นตอนที่ 3 มา mod 11 (หารเอาเศษ) จะได้ 205 mod 11 = 7
    let check1 = sumValue % 11

    // ขั้นตอนที่ 5 - เอา 11 ตั้ง ลบออกด้วย เลขที่ได้จากขั้นตอนที่ 4 จะได้ 11-7 = 4 (เราจะได้ 4 เป็นเลขในหลัก Check Digit)
    var check2 = 11 - check1

//    ถ้าเกิด ลบแล้วได้ออกมาเป็นเลข 2 หลัก ให้เอาเลขในหลักหน่วยมาเป็น Check Digit (เช่น 11 ให้เอา 1 มา, 10 ให้เอา 0 มา เป็นต้น)
    if check2 >= 10 {
        check2 -= 10
    }

    if check2 == bitCheck {
        return true
    } else {
        return false
    }
}

#endif
