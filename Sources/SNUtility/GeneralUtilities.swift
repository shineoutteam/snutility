//
//  GeneralUtilities.swift
//
//  Created by Saran Nonkamjan on 1/7/2561 BE.
//  Copyright © 2561 Saran Nonkamjan. All rights reserved.
//

#if canImport(UIKit)

import UIKit
import SNExtension

open class GeneralUtilities: NSObject {

    public class func dataFilePath(fname: String, directory: FileManager.SearchPathDirectory = .documentDirectory) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(directory, .userDomainMask, true)
        guard let path = paths.first else {
            return ""
        }
        let fullPath = path + fname

        GeneralUtilities.createDirectory(path: fullPath)
        return fullPath
    }

    public class func dataCacheFilePath(fname: String) -> String {
        return dataFilePath(fname: fname, directory: .cachesDirectory)
    }

    public class func createDirectory(path: String) {
        var cPath = path
        if cPath.NSStringValue.pathExtension != "" {
            cPath = cPath.NSStringValue.deletingLastPathComponent

            try? FileManager.default.createDirectory(atPath: cPath, withIntermediateDirectories: true, attributes: nil)
        }
    }

    public class var projectName: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
    }

    public class var projectDisplayName: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? ""
    }

    public class var projectVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
    }

    public class var projectBuild: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String ?? ""
    }

    public class func NSStringIsValidNumber(checkString: String) -> Bool {
        let stricterFilterString = "[0-9]*+$"//"[0-9\\-]"
        let webTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        return webTest.evaluate(with:checkString)
    }

    public class func NSStringIsValidEmail(checkString: String) -> Bool {
        let stricterFilter = true
        let stricterFilterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let laxString = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*"
        let emailRegex = (stricterFilter ? stricterFilterString : laxString)
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: checkString)
    }

    public class func NSStringIsValidWebSite(checkString: String) -> Bool {
//        guard let url = URL(string: checkString) else {
//            return false
//        }
//        return UIApplication.shared.canOpenURL(url)
        
//        let stricterFilterString = "^http(s?)\\:\\/\\/[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(\\/\\S*)?$"
//        let webTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
//        return webTest.evaluate(with:checkString)

        if checkString.hasPrefix("http://") || checkString.hasPrefix("https://") {
            return true
        } else {
            return false
        }
    }

    public class var isDebug: Bool {
        #if DEBUG
            return true
        #else
            return false
        #endif
    }

    public class var isProd: Bool {
        return !isDebug
    }
}

private var openLogFirst = true
public var isLogFile = true

public func print$(_ items: Any?...) {

    if items.count == 0 {
        print("")
    } else {
        for item in items {
            if item != nil {
                print(item!)
            }
        }
    }

    let save = "/so/log.txt"
    let savePath = GeneralUtilities.dataFilePath(fname: save, directory: .documentDirectory)

    if openLogFirst == true {
        try? FileManager.default.removeItem(atPath: savePath)
        openLogFirst = false
    }

    if isLogFile {
        var logString: String = ""
        do {
            logString = try String(contentsOfFile: savePath)
        } catch {
//            print("Load Error: \(error)")
        }

        if items.count == 0 {
            logString.append(contentsOf: "\n")
        } else {
//            logString.append(contentsOf: "\n" + Date().printFormatter(formatter: "yyyy-MM-dd HH:mm:ss Z", identifier: Calendar.Identifier.gregorian) + " " + items.debugDescription)

            logString.append(contentsOf: Date().printFormatter(formatter: "yyyy-MM-dd HH:mm:ss Z", identifier: Calendar.Identifier.gregorian) + " ")

            for item in items {
                if item != nil {
                    logString.append(contentsOf: "\(item!)\n")
                }
            }
        }

        do {
            try logString.write(toFile: savePath, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("Save Error: \(error)")
        }
    }
}

/*
<key>CFBundleDisplayName</key>
<string>Copper Trade</string>
<key>LSRequiresIPhoneOS</key>
<true/>
<key>LSSupportsOpeningDocumentsInPlace</key>
<true/>
 */

#endif
