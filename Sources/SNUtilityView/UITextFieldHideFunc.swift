//
//  UITextFieldHideFunc.swift
//
//  Created by Saran Nonkamjan on 1/7/2561 BE.
//  Copyright © 2561 Saran Nonkamjan. All rights reserved.
//

#if canImport(UIKit)

import UIKit

open class UITextFieldHideFunc: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
//            return true
//        }

        return false;
    }
}

#endif
