//
//  CornerView.swift
//
//  Created by Saran Nonkamjan on 10/7/2561 BE.
//  Copyright © 2561 Saran Nonkamjan. All rights reserved.
//

#if canImport(UIKit)

import UIKit

open class CornerView: UIView {
    override public func prepareForInterfaceBuilder() {
        initialize()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        initialize()
    }

    @IBInspectable public var borderColor: UIColor? {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var gradientColor: UIColor? {
        didSet {
            initialize()
        }
    }

    @IBInspectable public var gradientHorizental: Bool = false {
        didSet {
            self.initialize()
        }
    }

    private var gradient: CAGradientLayer?

    @IBInspectable public var isCycle: Bool = false {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var corner: CGFloat = 0.0 {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet {
            self.initialize()
        }
    }

    public func initialize() {
        layer.cornerRadius = corner
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth

        if isCycle {
            layer.cornerRadius = frame.size.height / 2
        }

        if let gradientColor = gradientColor {
            if (gradient == nil) {
                gradient = CAGradientLayer()
            }

            if let gradient = gradient {
                gradient.frame = bounds
                gradient.colors = [gradientColor.cgColor, backgroundColor?.cgColor ?? UIColor.white]

                if gradientHorizental {
                    gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
                    gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
                }

                layer.insertSublayer(gradient, at: 0)
            }
        }
    }
}

open class CornerImageView: UIImageView {
    override public func prepareForInterfaceBuilder() {
        initialize()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        initialize()
    }

    @IBInspectable public var borderColor: UIColor? {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var isCycle: Bool = false {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var corner: CGFloat = 0.0 {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet {
            self.initialize()
        }
    }

    public func initialize() {
        layer.cornerRadius = corner
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth

        if isCycle {
            layer.cornerRadius = frame.size.height / 2
        }
    }
}

open class CornerButtonView: UIButton {

    @IBInspectable public var isCycle: Bool = false {
        didSet {
            if isCycle {
                layer.cornerRadius = frame.size.height / 2
            }
        }
    }

    @IBInspectable public var corner: CGFloat = 0.0 {
        didSet {
            if isCycle {
                layer.cornerRadius = frame.size.height / 2
            } else {
                layer.cornerRadius = corner
            }
        }
    }

    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

    @IBInspectable public var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

    @IBInspectable public var shadowColor: UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor

        }
    }

    @IBInspectable public var shadowOpacity: CGFloat = 0.0 {
        didSet {
            layer.shadowOpacity = Float(shadowOpacity)
        }
    }

    @IBInspectable public var shadowRadius: CGFloat = 0.0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }

    @IBInspectable public var gradientColor: UIColor? {
        didSet {
            if let gradientColor = gradientColor {
                if (gradient == nil) {
                    gradient = CAGradientLayer()
                }

                if let gradient = gradient {
                    gradient.frame = bounds
                    gradient.colors = [gradientColor.cgColor, backgroundColor?.cgColor ?? UIColor.white]

                    layer.insertSublayer(gradient, at: 0)
                }
            }
        }
    }

    @IBInspectable public var hightlightColor: UIColor? {
        didSet {
            initialize()
        }
    }

    private var mainBGColor: UIColor?
    private var gradient: CAGradientLayer?

    override public func prepareForInterfaceBuilder() {
        initialize()
    }

    override public var isHighlighted: Bool {
        didSet {
            super.isHighlighted = isHighlighted

            if hightlightColor == nil {
            } else {
                if mainBGColor == nil {
                    mainBGColor = backgroundColor
                }

                if isHighlighted {
                    backgroundColor = hightlightColor
                } else {
                    backgroundColor = mainBGColor
                }
            }
        }
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        initialize()
    }

    public func initialize() {
        layer.shadowOffset = .zero

        if isCycle == true {
            isCycle = true
        }
    }
}

open class CornerLabelView: UILabel {
    override public func prepareForInterfaceBuilder() {
        initialize()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        initialize()
    }

    @IBInspectable public var borderColor: UIColor = .clear {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var isCycle: Bool = false {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var corner: CGFloat = 0.0 {
        didSet {
            self.initialize()
        }
    }

    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet {
            self.initialize()
        }
    }

    public func initialize() {
        layer.cornerRadius = corner
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth

        if isCycle {
            layer.cornerRadius = frame.size.height / 2
        }
    }
}

open class CornerTextField: UITextField {

    @IBInspectable public var corner: CGFloat = 0.0 { didSet { initialize() } }

    @IBInspectable public var borderWidth: CGFloat = 0.0 { didSet { initialize() } }

    @IBInspectable public var borderColor: UIColor? { didSet { initialize() } }

    @IBInspectable public var placeHolderTextColor: UIColor? { didSet { initialize() } }

    @IBInspectable public var leftSpace: CGFloat = 0.0 {
        didSet {
            guard leftSpace != 0 else { return }
            leftView = UIView(frame: CGRect(x: 0, y: 0, width: leftSpace, height: 10))
        }
    }

    @IBInspectable public var rightSpace: CGFloat = 0.0 {
        didSet {
            guard rightSpace != 0 else { return }
            rightView = UIView(frame: CGRect(x: 0, y: 0, width: rightSpace, height: 10))
        }
    }

    @IBInspectable public var hideFunction: Bool = false { didSet { initialize() } }

    override public func prepareForInterfaceBuilder() {
        initialize()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        initialize()
    }

    public func initialize() {
        layer.cornerRadius = corner
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: placeHolderTextColor ?? .black])

        leftViewMode = .always
        rightViewMode = .always
    }

    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
//            return true
//        }
        if hideFunction == true {
            return false
        } else {
            return super.canPerformAction(action, withSender: sender)
        }
    }
}

open class CornerTextView: UITextView {

    @IBInspectable public var corner: CGFloat = 0.0 { didSet { initialize() } }

    @IBInspectable public var borderWidth: CGFloat = 0.0 { didSet { initialize() } }

    @IBInspectable var borderColor: UIColor? { didSet { initialize() } }

    @IBInspectable public var placeHolderTextColor: UIColor? { didSet { initialize() } }

    @IBInspectable public var hideFunction: Bool = false { didSet { initialize() } }

    override public func prepareForInterfaceBuilder() {
        initialize()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        initialize()
    }

    public func initialize() {
        layer.cornerRadius = corner
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
//        setValue(placeHolderTextColor, forKey: "_placeholderLabel.textColor")
    }

    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if hideFunction == true {
            return false
        } else {
            return super.canPerformAction(action, withSender: sender)
        }
    }
}

#endif
