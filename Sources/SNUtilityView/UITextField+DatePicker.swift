//
//  UITextField+DatePicker.swift
//
//  Created by Saran Nonkamjan on 1/20/2559 BE.
//  Copyright © 2559 Saran Nonkamjan. All rights reserved.
//

#if canImport(UIKit)
#if os(iOS)

import UIKit
import SNLanguage

open class UITextField_DatePicker: CornerTextField {

    @IBInspectable public var dateFormat: String! = "yyyy-MM-dd"
    public var calendarIdentifier: Calendar.Identifier = .gregorian

    @IBInspectable public var date: Date? {
        didSet {
            self.text = stringDate()
            if date != nil {
                self.datePicker.date = date!
            }
        }
    }

    public var datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 320, height: 216))
    public let toolBar = UIToolbar()

    @IBInspectable public var doneBackgroundColor: UIColor! = UIColor.black {
        didSet {
            toolBar.barTintColor = doneBackgroundColor
        }
    }

    @IBInspectable public var doneTextColor: UIColor! = UIColor.white {
        didSet {
            let doneButton = toolBar.items?.first(where: { return ($0.tag == 10) })
            doneButton?.tintColor = doneTextColor
        }
    }

    override public func awakeFromNib() {
        self.inputView = datePicker

        if Langauge.sharedObject.langauge == LangaugeType.th {
            calendarIdentifier = .buddhist
        } else {
            calendarIdentifier = .gregorian
        }

        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = doneBackgroundColor
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneSelect))
        doneButton.tag = 10
        doneButton.tintColor = doneTextColor
        doneButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)], for: .normal)
        doneButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)], for: .highlighted)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        self.inputAccessoryView = toolBar

        let calendar = Calendar(identifier:calendarIdentifier)
        datePicker.calendar = calendar
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: Langauge.sharedObject.langauge.localizedString)
        datePicker.maximumDate = Date()

        self.text = stringDate()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

    }
    
    public func updateLanguage() {
        awakeFromNib()
    }

    fileprivate func stringDate() -> String {
        if date == nil {
            return ""
        }

        let formatter = DateFormatter()
        let calendar = Calendar(identifier:calendarIdentifier)
        formatter.calendar = calendar
        formatter.dateFormat = dateFormat

        return formatter.string(from: date!)
    }

    @objc func doneSelect() {
        date = datePicker.date
        resignFirstResponder()
    }
}

#endif
#endif
