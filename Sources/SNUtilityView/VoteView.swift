//
//  VoteView.swift
//
//  Created by Saran Nonkamjan on 1/5/2559 BE.
//  Copyright © 2559 Saran Nonkamjan. All rights reserved.
//

#if canImport(UIKit)

import UIKit

public protocol VoteViewDelegate {
    func voteViewUpdate(_ voteView: VoteView)
}

@IBDesignable
open class VoteView: UIView {

    @IBInspectable public var imageEmpty: String?
    @IBInspectable public var imageFull: String?
    @IBInspectable public var imageHalf: String?

    @IBInspectable public var point: CGFloat = 0 {
        didSet {
            print("Point: ", point)
            updateStar(point)
        }
    }
    @IBInspectable public var maxPoint: NSInteger = 5

    public var delegate: VoteViewDelegate?

    override public func prepareForInterfaceBuilder() {
        initialize()

        backgroundColor = UIColor.white
    }

    override public func layoutSubviews() {
        super.layoutSubviews()

        initialize()
    }

    public func initialize() {
        updateStar(point)
    }

    public func updateStar(_ point: CGFloat) {

        backgroundColor = UIColor.clear

        // Create Star
        if (viewWithTag(1) == nil) {
            for index in 1...maxPoint {
                let star = UIImageView(image: UIImage(named: imageEmpty ?? ""))
                star.tag = index
                star.contentMode = .scaleAspectFit
                addSubview(star)
            }
        }

        // Config Position
        let widthStar = frame.size.width / CGFloat(maxPoint)

        for index in 1...maxPoint {
            guard let star = viewWithTag(index) else {
                continue
            }
            let position = index-1

            star.frame = CGRect(x: CGFloat(position)*widthStar, y: 0, width: widthStar, height: frame.size.height)
        }

        // Set Star
        var cacheStar = point
        for index in 1...maxPoint {
            guard let star = viewWithTag(index) as? UIImageView else {
                continue
            }

            if cacheStar >= 1 {
                star.image = UIImage(named: imageFull ?? "")
            } else if cacheStar > 0 {
                if imageHalf == nil {
                    star.image = UIImage(named: imageEmpty ?? "")
                } else {
                    star.image = UIImage(named: imageHalf ?? "")
                }
            } else {
                star.image = UIImage(named: imageEmpty ?? "")
            }

            cacheStar -= 1
        }

        delegate?.voteViewUpdate(self)
    }

    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        updateVote(touches.first)
    }

    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        updateVote(touches.first)
    }

    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print( (touches.first?.location(in: self).x) ?? -9999999)
        updateVote(touches.first)
    }

    private func updateVote(_ touch: UITouch?) {
        guard let firstTouch = touch else {
            return
        }

        let widthStar = frame.size.width / CGFloat(maxPoint)
        let newPoint = firstTouch.location(in: self)

        var positionX = newPoint.x
        if positionX < 0 {
            positionX = 0
        } else if positionX > frame.size.width-widthStar {
            positionX = frame.size.width-widthStar
        }

        let votePoint = (positionX/widthStar) + 1

        point = votePoint
    }
}

#endif
